<?php
/**
 * Template Name: About Page Template
 * The template for displaying about page.
 *
 * @package WordPress
 * @subpackage Gown_for_Rent
 * @since Gown for Rent 1.0
 */
get_header(); ?>

<section class="about">
	<!-- Start of Banner -->
	<article class="banner-content-section inner">
		<div id="banner">
			<div class="placeholder-bg">
				<div class="banner-item" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner-inner-page.jpg); background-size: cover; background-position: center;">
					<div class="mask-overlay"></div>
					<div class="banner-captions">
						<div class="container">
							<div class="col-xs-12 text-center">
								<h2 class="italic">About</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
	<!-- End of Banner -->
	<!-- Start of Letter for Clients -->
	<article class="single-content-section letter-for-clients-section pad-50">
		<p>Roxy Roco, founder of customdesigndress.com was keen enough to address a new demand arising from her wide range of pre-ordered custom-made gowns. People began to ask “Do you have gowns for rent?” Roxy Roco did not say no. She wanted to be The Answer. Thus in 2012, gownforrent.com was born from the desire of clients to look great without burning the bank. She realized this was very sustainable and economical after seeing a gown worn by several people for different occasions. Also, people’s mindset is changing. They no longer want to keep gowns in their closet especially if it’s for one-time use. It’s clutter, a space-waster, a dust-collector and a headache to keep a big gown only used during special occasions.</p>

		<p class="pad-t-30">There is a gown for everyone. We have had clients who can afford an extravagant wedding but choose to rent their gown. Yes we’ve even had the kind of clients who wear crystal jewelry, carry a branded bag by their yaya and exclaim “Pangatlong gown ko na to! I can’t make up my mind which gown to wear tonight!” And of course we also have the very stingy down-to-every-last-centavo-type of clients. Who is OUR TRAGET? Basically, anyone who lives a practical lifestyle.</p>
		 
		<p class="pad-t-30">What is great is that they know what they are wearing, it is presented to them on-the-spot – they don’t have to pre-order a gown and get what they’re not expecting.</p>
		 
		<p class="pad-t-30">Our fitting is a breeze in the park. All of our gowns are back corset style which means this can be adjusted to fit any body frame.</p>
		 
		<p class="pad-t-30">Our greatest achievement is providing fast and reliable service to our clients. We rush to assist and comfort problematic brides (whose gown design wasn’t followed, or doesn’t fit right). They try on our gowns and fall instantly in love with everything. Last minute events are no hassle for the busy bee. They view all our designs online and book instantly. It’s common for a walk-in to rent on the spot, take the gown, and attend an event the same evening!</p>
		
		<p class="pad-t-30">In the Philippines we’re definitely easy to reach (Viber, Instagram, Whatsapp, Telegram, Line, Messenger, Email, Live chat on our website).</p>
		 
		<p class="pad-t-30">We have more than 200 gowns. We aim to have half a million gowns and own a hotel-like library of gowns by 2018. We’re developing an online platform that will allow clients to make apt reservations, gown bookings and payments directly online.</p>
		 
		<p class="pad-t-30">So, for time, energy and budget’s sake “why buy when you can rent?” Happy renting at gownsforrent.com!</p>

		<p class="pad-t-30">*We follow strict quality-control protocol when it comes to hygiene. Gowns are all properly handled and dry-cleaned before rented.</p>
	</article>
	<!-- End of Letter for Clients -->
	<!-- Start of Our Team-->
	<article class="content-section our-team m-t-100 m-b-100">
		<h3 class="italic text-center">Meet the Team</h3>
		<div class="container m-t-20">
			<div class="col-sm-12 col-md-4">
				<div class="employee-wrapper">
					<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2016/08/roxy-roco.jpg" alt="">
					<div class="employee-details">
						<h3 class="text-center no-m-b">Roxy Roco</h3>
						<p>Founder</p>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-4">
				<div class="employee-wrapper">
					<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2016/08/marlyn-baron.jpg" alt="">
					<div class="employee-details">
						<h3 class="text-center no-m-b">Marlyn Baron</h3>
						<p>Sales Director</p>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-4">
				<div class="employee-wrapper">
					<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2016/08/mim-cabrera.jpg" alt="">
					<div class="employee-details">
						<h3 class="text-center no-m-b">Mim Cabrera</h3>
						<p>Web Design and Technology Officer</p>
					</div>
				</div>
			</div>
		</div>
	</article>
	<!-- End of Our Team-->
	<!-- Start of Contact Us-->
	<article class="content-section contact-us pad-30" itemscope itemtype="http://schema.org/LocalBusiness">
		<div class="container m-t-20">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="row">
						<div class="col-xs-2"><h2 class="italic">01/</h2></div>
						<div class="col-xs-10"><h3 class="italic">Get in Touch</h3></div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p>We're very approachable and would like to speak to you. Feel free to call, send us an email, Tweet us or simply complete the inquiry form.</p>
							<ul>
								<li>
									<div class="row">
										<div class="col-xs-1 text-center">
											<i class="fa fa-clock-o"></i>
										</div>
										<div class="col-xs-11">
											<p itemprop="openingHours" content="Mo,Tu,We,Fr,Sa, Su 9:00-18:00" datetime="Mo, Tu, We, Th, Fr, Sa, Su 09:00-18:00">Opening Hours : Mon-Fri 9AM to 8PM | Sat 9AM to 6PM | Sun 1PM to 6PM</p>
											<p>Holidays : Upon <a href="//fb.com/manilagowns/">Facebook</a> Announcement</p>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-xs-1 text-center">
											<i class="fa fa-phone"></i>
										</div>
										<div class="col-xs-11">
											<a itemprop="telephone" content="+63229904957" href="tel:029904957">990 4957</a> / <a itemprop="telephone" content="+6327821180" href="tel:027821180">782 1180</a>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-xs-1 text-center">
											<i class="fa fa-mobile"></i>
										</div>
										<div class="col-xs-11">
											<a itemprop="telephone" content="+6329983606102" href="tel:09983606102">Smart - 09983606102</a>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-xs-1 text-center">
											<i class="fa fa-mobile"></i>
										</div>
										<div class="col-xs-11">
											<a itemprop="telephone" content="+6329174178891" href="tel:09174178891">Globe - 09174178891</a>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-xs-1 text-center">
											<i class="fa fa-mobile"></i>
										</div>
										<div class="col-xs-11">
											<a itemprop="telephone" content="+639209186466" href="tel:09209186466">(Viber, Telegram, Whatsapp, LINE) 09209186466</a>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-xs-1 text-center">
											<i class="fa fa-envelope"></i>
										</div>
										<div class="col-xs-11">
											<a itemprop="email" content="sales@gownforrent.com" href="mailto:sales@gownforrent.com">sales@gownforrent.com</a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="row">
						<div class="col-xs-2"><h2 class="italic">02/</h2></div>
						<div class="col-xs-10"><h3 class="italic">Send us a Message</h3></div>
						<?php echo do_shortcode('[contact-form-7 id="26" title="Contact Us"]') ?>
					</div>
				</div>
			</div>
			<div class="row m-t-50">
				<div class="col-xs-12">
					<div class="row m-b-20">
						<div class="col-xs-1"><h2 class="italic">03/</h2></div>
						<div class="col-xs-11"><h3 class="italic">Visit us</h3></div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3859.8497431075625!2d121.04120195039782!3d14.664467179389629!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTTCsDM5JzUyLjEiTiAxMjHCsDAyJzM2LjIiRQ!5e0!3m2!1sen!2sph!4v1467614801196" width="1920" height="500" frameborder="0" style="border:0; display: block; margin: auto" allowfullscreen></iframe>
							<a href="waze://?ll=14.664462,121.043396" target="_blank">Open in Waze</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
	<!-- End of Contact Us-->
<?php get_footer(); ?>
