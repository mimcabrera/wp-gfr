<?php
/**
* Template Name: Homepage Template
* The template for displaying about page.
*
* @package WordPress
* @subpackage Gown_for_Rent
* @since Gown for Rent 1.0
*/
get_header(); ?>

<section class="homepage">
  <!-- Start of Banner -->
  <article class="banner-content-section">
    <a href="/collections">
      <div id="banner">
        <div class="placeholder-bg">
            <?php while( have_rows('banner_images', 'option') ): the_row(); ?>
              
                <div class="banner-item" style="background: url(<?php the_sub_field('image'); ?>); background-size: cover; background-position: top right;">         
            <?php endwhile; ?>
            </div>
        </div>
      </div>
    </a>
  </article>
  <!-- End of Banner -->

  <!-- Start of GFR -->
  <article class="single-content-section gfr-section pad-50 text-center">
    <h2 class="italic">Gown For Rent</h2>
    <p>Have fun dressing up! We want to provide you with affordable and convenient ways to finding your dress! In today’s generation we understand your approach to dressing up – save time, be practical, and look beautiful! Visit our showroom or have your items shipped!</p>
    <a href="<?php echo get_site_url() ?>/about" class="btn-gfr-default center dark-gray sm m-t-20">Read more</a>
  </article>
  <!-- End of GFR -->
  <!-- Start of Featured Collection -->
  <article class="grid-content-section featured-collection pad-t-50 pad-b-50">
    <h2 class="italic text-center">Premium Collection</h2>
    <span class="line-divider"></span>
    <div class="container">
      <div id="featured-collection">
        <?php
        $posts = get_posts(array(
          'posts_per_page'	=> 8,
          'post_type'			=> 'gowns',
          'orderby'			=> 'title',
          'order'				=> 'ASC',
          'meta_key'			=> 'price',
          'meta_value'		=> '5000',
          'meta_compare' 		=> '>=',
        ));
        if( $posts ): ?>
        <?php foreach( $posts as $post ):
          setup_postdata( $post )
          ?>
          <?php
          $thumbnail_image = get_field('thumbnail_image', $post->ID);
          $price = get_field('price', $post->ID);
          ?>
          <div class="grid-item-container">
            <a href="<?php the_permalink(); ?>">
              <div class="grid-item-image" style="background: url(<?php echo $thumbnail_image; ?>) #eee; max-width: 100%; height: 47vw; background-size: cover; background-position: center;">
              </div>
              <div class="grid-item-description">
                <div class="col-xs-12 no-pad text-center">
                  <h5 class="italic no-m"><?php the_title(); ?></h5>
                  <?php if ( get_field( 'old_price' ) ): ?>
                    <s><p class="price" style="color:#777">Php <?php the_field('old_price'); ?></p></s>
                    <?php
                    else :
                      // no old price found
                    endif;
                    ?>
                    <p class="price">Php <?php echo $price; ?></p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <a href="<?php the_permalink(); ?>" class="btn-gfr-default dark-gray xs center xs-rent m-t-5 ">Rent Now</a>
                  </div>
                </div>
              </a>
            </div>
          <?php endforeach; ?>
          <?php wp_reset_postdata(); ?>
        <?php endif; ?>
      </div>
    </div>
  </article>
  <!-- End of Featured Collection -->
  <!-- Start of Why Rent a Gown -->
  <article class="single-content-section why-rent-a-gown-section pad-50 text-center">
    <h2 class="italic">Why Rent a Gown?</h2>
    <p>A formal dress can be pricey. And after using it, chances are you wont wear it often enough (or never again) to get back what you paid. So why rent? Because it’s sustainable! Not only for the environment, but for your budget as well! We have dresses as low as Php300. It’s Easy! You have options! And we handle dry cleaning!</p>
    <a href="<?php echo get_site_url() ?>/collections" class="btn-gfr-default center dark-gray sm m-t-20">Rent now</a>
  </article>
  <!-- End of Why Rent a Gown -->
  <!-- Start of Month's Favorite -->
  <article class="grid-content-section month-favorite-section pad-t-100 pad-b-100">
    <h2 class="monthly-greetings text-center"><?php the_field('monthly-greetings', 'option'); ?></h2>
    <span class="line-divider-decorative text-center"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/monthly-greetings-separator.png" alt=""></span>
    <h3 class="italic text-center m-b-20">This Month’s Favorites</h3>
    <div class="container m-t-20">
      <div class="col-sm-12 col-md-6">
        <?php
        $posts = get_posts(array(
          'posts_per_page'	=> 1,
          'post_type'			=> 'gowns',
          'category_name'     => 'this-month-gown',
        ));
        if( $posts ): ?>
        <?php foreach( $posts as $post ):
          setup_postdata( $post )
          ?>
          <?php
          $thumbnail_image = get_field('thumbnail_image', $post->ID);
          $price = get_field('price', $post->ID);
          ?>
          <a href="<?php the_permalink(); ?>">
            <div class="grid-item-container">
              <div class="grid-item-lg" style="background: url(<?php echo $thumbnail_image; ?>); background-size: cover; background-position: top center">
                <div class="grid-item-description">
                  <p class="price">Php <?php echo $price; ?></p>
                </div>
              </div>
            </div>
          </a>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>
    </div>
    <div class="col-sm-12 col-md-6">
      <div class="col-lg-12 no-pad">
        <div class="row m-b-30">
          <?php
          $posts = get_posts(array(
            'posts_per_page'	=> 4,
            'post_type'				=> 'gowns',
            'category_name'   => 'this-month-gown-small',
          ));
          if( $posts ): ?>
          <?php foreach( $posts as $post ):
            setup_postdata( $post )
            ?>
            <?php
            $thumbnail_image = get_field('thumbnail_image', $post->ID);
            $price = get_field('price', $post->ID);
            ?>
            <a href="<?php the_permalink(); ?>">
              <div class="col-xs-6">
                <div class="grid-item-container">
                  <div class="grid-item-sm" style="background: url(<?php echo $thumbnail_image; ?>); background-size: cover; background-position: top center">
                    <div class="grid-item-description">
                      <p class="price">Php <?php echo $price; ?></p>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          <?php endforeach; ?>
          <?php wp_reset_postdata(); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
</article>
<!-- End of Month's Favorite -->
<!-- Start of Collections -->
<article class="content-section collections-section pad-50">
  <div class="container">
    <div class="col-xs-6">
      <h4>Accessories</h4>
      <a href="#" class="btn-gfr-line">See All Collections</a>
    </div>
    <div class="col-xs-6">
      <h4>Bridal Gown</h4>
      <a href="#" class="btn-gfr-line">See All Collections</a>
    </div>
  </div>
</article>
<!-- End of Collections -->
<!-- Start of Testimonials -->
<article class="content-section testimonials-section pad-t-50 pad-b-50 text-center"">
  <div class="container">
    <div id="review-container">
      <div class="review">
        <div class="col-xs-12 m-b-20">
          <div class="placeholder" data-large="<?php echo get_site_url(); ?>/wp-content/uploads/2016/08/marichu-quintal.png">
            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2016/08/marichu-quintal-thumb.png" class="img-small" alt="">
            <div style="padding-bottom: 123%;"></div>
          </div>
        </div>
        <div class="col-xs-12">
          <h4>Marichu Quintal</h4>
          <p class="m-w-700">Gown for rent's gowns has its latest unique designs that would suit our taste, considering the budget also which is acceptable. Sellers are friendly and easy to talk regarding with concerns and suggestions more powers! Thank you and God bless</p>
          <a href="<?php echo get_site_url() ?>/testimonials" class="btn-gfr-default dark-gray sm m-t-20">See all</a>
        </div>
      </div>
    </div>
  </div>
</article>
<!-- End of Testimonials -->
<!-- Start of Rent a Gown -->
<article class="content-section how-to-rent-section pad-t-100 pad-b-100" id="how-to-rent">
  <div class="container">
    <div class="col-sm-12 col-md-6 col-md-push-6 text-center">
      <div class="placeholder" data-large="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/how-to-rent-a-gown.png">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/thumb/how-to-rent-a-gown.png" class="img-small" alt="">
        <div style="padding-bottom: 100%;"></div>
      </div>
    </div>
    <div class="col-sm-12 col-md-6 col-md-pull-6">
      <div class="text-center">
        <h3 class="italic">How to Rent a Gown?</h3>
        <span class="line-divider"></span>
      </div>
      <div class="row pad-b-20">
        <div class="col-xs-2"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2016/08/dress.png" class="m-l-20" width="50px" alt=""></div>
        <div class="col-xs-10"><p class="pad-t-10">Select your preferred dress and accessories</p></div>
      </div>
      <div class="row pad-b-20">
        <div class="col-xs-2"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2016/08/measure.png" class="m-l-20" width="50px" alt=""></div>
        <div class="col-xs-10"><p class="pad-t-10">Check its availability and size</p></div>
      </div>
      <div class="row pad-b-20">
        <div class="col-xs-2"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2016/08/calendar.png" class="m-l-20" width="50px" alt=""></div>
        <div class="col-xs-10"><p class="pad-t-10">Reserve it! Book your appointment for fitting!</p></div>
      </div>
      <a href="<?php echo get_site_url() ?>/collections" class="btn-gfr-default blue sm center">Rent now!</a>
    </div>
  </div>
</article>
<!-- End of Rent a Gown -->
<?php get_footer(); ?>
