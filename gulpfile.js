var gulp 			= require('gulp');
var sass 			= require('gulp-sass');
var cssnano 		= require('gulp-cssnano');
var uglify 			= require('gulp-uglify');
var rename 			= require('gulp-rename');
var concat 			= require('gulp-concat');
var sourcemaps 		= require('gulp-sourcemaps');
var autoprefixer 	= require('gulp-autoprefixer');
var livereload 		= require('gulp-livereload');

gulp.task('sass', function () {
	gulp.src('./assets/components/sass/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(cssnano())
		.pipe(sourcemaps.write('./'))

	.pipe(gulp.dest('./assets/css/'))
	.pipe(livereload());
});

gulp.task('cssmin', function () {
	gulp.src('./assets/components/sass/**/*.css')
		.pipe(sourcemaps.init())
		.pipe(cssnano())
		.pipe(sourcemaps.write('./'))

	.pipe(gulp.dest('./assets/css/'))
	.pipe(livereload());
});

gulp.task('script', function() {
  gulp.src('./assets/components/js/*.js')
  	.pipe(concat('app.js'))
	.pipe(rename({ suffix: '.min' }))	
	.pipe(uglify())
	.pipe(gulp.dest('./assets/js/'))
	.pipe(livereload());
});

gulp.task('php', function () {
	gulp.src('./*.php')
	.pipe(livereload());
});

gulp.task('watch', function() {
	  gulp.watch(['./assets/components/sass/**/*.css'], ['cssmin']);
	  gulp.watch(['./assets/components/**/*.scss'], ['sass']);
	  gulp.watch(['./assets/components/js/*.js'], ['script']);
	  gulp.watch(['./*.php'], ['php']);
	  livereload.listen();
});
 
// Default task
gulp.task('default', ['watch']);