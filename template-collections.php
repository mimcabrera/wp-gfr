<?php
/**
 * Template Name: Collections Page Template
 * The template for displaying about page.
 *
 * @package WordPress
 * @subpackage Gown_for_Rent
 * @since Gown for Rent 1.0
 */
get_header('collection'); ?>
	<!-- Start of Collection-->
	<article class="content-section collection-section">
		<div class="container-fluid">
			<div class="col-md-2">
				<div class="sidebar-collection hidden-xs hidden-sm">
					<div class="search">
						<form action="<?php echo home_url(); ?>" id="search-form" method="get">
						    <input type="text" name="s" id="s" placeholder="Search" value="" onblur="if(this.value=='')this.value=''"
						    onfocus="if(this.value=='')this.value=''" />
						    <input type="hidden" value="submit" />
						</form>
					</div>
					<h5 class="italic">Gowns</h5>
					<ul class="no-m">
						<li><a href="<?php echo home_url(); ?>/collections">All</a></li>
					</ul>
					<?php
						$post_type		= 'gowns';
						$taxonomy		= 'category';
						$orderby		= 'ASC';
						$show_count		= 0;
						$hide_empty		= 0;
						$pad_counts		= 0;
						$hierarchical	= 1;
						$exclude		= '1,24,28,29,42';
						$title			= ' ';

							$args = array(
								'post_type'			=> $post_type,
								'taxonomy'			=> $taxonomy,
								'orderby'			=> $orderby,
								'show_count'		=> $show_count,
								'hide_empty'		=> $hide_empty,
								'pad_counts'		=> $pad_counts,
								'hierarchical'	    => $hierarchical,
								'title_li'			=> $title,
								'exclude'			=> $exclude
							);

					wp_list_categories( $args )

					?>
					<h5 class="italic">Accessories</h5>
					<?php
						$post_type		= 'accessories';
						$taxonomy		= 'accessories_category';
						$orderby		= 'ASC';
						$show_count		= 0;
						$hide_empty		= 0;
						$pad_counts		= 0;
						$hierarchical	= 1;
						$title			= ' ';

							$args = array(
								'post_type'			=> $post_type,
								'taxonomy'			=> $taxonomy,
								'orderby'			=> $orderby,
								'show_count'		=> $show_count,
								'hide_empty'		=> $hide_empty,
								'pad_counts'		=> $pad_counts,
								'hierarchical'	    => $hierarchical,
								'title_li'			=> $title
							);

					wp_list_categories( $args )

					?>
					<a href="/category/exclusive-sale/"><h5 class="italic">Exclusive Sale</h5></a>
					<h5 class="italic">Gift Certificate</h5>
				</div>
			</div>
			<div class="col-md-10">
				<div class="collection-wrapper">
					<a class="toggle-button btn-gfr-default blue square">Open Categories</a>
					<!-- Start of Collection Items -->
					<article class="grid-content-section collection-items-section">
						<div class="row">
							<?php if (have_posts()) : ?>
								<?php while (have_posts()) : the_post(); ?>
									<?php the_content(); ?>

								<?php endwhile; ?>
							<?php else : ?>
							<?php endif; ?>
						</div>
					</article>
					<!-- End of Collection Items -->
				</div>
			</div>
		</div>
	</article>
	<!-- End of Collection-->
<?php get_footer(); ?>
