<?php
/**
 * Template Name: Rent a Gown Page Template
 * The template for displaying about page.
 *
 * @package WordPress
 * @subpackage Gown_for_Rent
 * @since Gown for Rent 1.0
 */

$productid 	= $_GET['product'];
$type 		= $_GET['type'];

get_header(); ?>
<section class="rent-a-gown">
	<!-- Start of Banner -->
	<article class="banner-content-section inner">
		<div id="banner">
			<div class="placeholder-bg">
				<div class="banner-item" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner-inner-page.jpg); background-size: cover; background-position: center;">
					<div class="mask-overlay"></div>
					<div class="banner-captions">
						<div class="container">
							<div class="col-xs-12 text-center">
								<h2 class="italic">Rent a Gown</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
	<!-- End of Banner -->
		<article class="form-content-section">
		<div class="product-details-wrapper m-b-50 container">
			<div class="row">
				<div class="col-xs-12">
					<?php
						if (!empty($_GET["product"]) && !empty($_GET["type"])):
							$posts = get_posts(array(
								'p'					=> $productid,
								'post_type'			=> $type
							));
							if( $posts ): ?>
								<?php foreach( $posts as $post ):
									setup_postdata( $post )
								?>
								<?php
									$title = $post->post_title;
									$thumbnail_image = get_field('thumbnail_image', $post->ID);
									$price = get_field('price', $post->ID);
								?>
								<div class="row">
									<div class="col-sm-12 col-md-3 text-center" style="margin-bottom: 20px">
										<img src="<?php echo $thumbnail_image; ?>" alt="">
									</div>
									<div class="col-sm-12 col-md-9">
										<div class="grid-item-description">
											<div class="col-xs-12 no-pad">
												<h4>Product Details</h4>
												<p class="no-m"> <strong>Name:</strong> <?php the_title(); ?></p>
												<p class="no-m"> <strong>Categories:</strong>  <?php the_category(' '); ?></p>
												<p class="price"><strong>Price:</strong>  Php <?php echo $price; ?></p>
												<br/>
												<h5>Policy</h5>
												<p>As a form of security, assurance and safety for out released item/s, gownforrent.com requires a refundable deposit upon pick up of the reserved item.</p>
												<p>For Gift Coupons, Vouchers, Raffle Winners, and other freebies/promos, a refundable deposit will be twice the amount.</p>
												<p>For Packages, Refundable Deposit of equal value and Dry-cleaning are required as stated on the contract.</p>
												<h5>Rates</h5>
												<p style="margin: 0">PHP 2000 Refundable Deposit for every gown 3,000 and below</p>
												<p style="margin: 0">PHP 3000 Refundable Deposit for every gown 3,500 and above</p>
												<p style="margin: 0">PHP 5000 Refundable Deposit for every gown 5,000 and above</p>
												<p>PHP 6000 Refundable Deposit for every gown 6,000 and above</p>
												<p style="font-style:italic; font-size: 0.7rem">**For Raffles Refundable Deposit and Dry-cleaning is Mandatory.</p>
												<h5>Dry Cleaning</h5>
												<p style="margin: 0">PHP 1000 Dry - Cleaning fee is deducted on refundable deposit (For Premium and Long Trail Gowns)</p>
												<p style="margin: 0">PHP 500 Dry - Cleaning fee is deducted on refundable deposit (For Teens and Adult Gowns) </p>
												<p>PHP 300 Dry - Cleaning fee is deducted on refundable deposit (For Kids Gowns) </p>
												<p style="font-style:italic; font-size: 0.7rem">**Rates are subject to change without prior notice</p>
											</div>
										</div>
									</div>
								</div>
								<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="container">
			<?php echo do_shortcode('[contact-form-7 id="45" title="Rent a gown"]') ?>
		</div>
	</article>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>
	<?php endif; ?>
</section>
<?php get_footer(); ?>
