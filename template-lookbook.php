<?php
/**
 * Template Name: Lookbook Page Template
 * The template for displaying about page.
 *
 * @package WordPress
 * @subpackage Gown_for_Rent
 * @since Gown for Rent 1.0
 */
get_header(); ?>

	<!-- Start of Banner -->
	<article class="banner-content-section inner">
		<div id="banner">
			<div class="placeholder-bg">
				<div class="banner-item" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner-inner-page.jpg); background-size: cover; background-position: center;">
					<div class="mask-overlay"></div>
					<div class="banner-captions">
						<div class="container">
							<div class="col-xs-12 text-center">
								<h2 class="italic">Lookbook</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
	<!-- End of Banner -->
<section class="lookbook">
	<!-- Start of Lookbook-->
	<article class="content-section lookbook-section pad-30">
		<p class="text-center">Awesome video's are coming soon! </br>Watch out for them!</p>
		<p class="text-center"><i class="fa fa-video-camera"></i></p>
	</article>
	<!-- End of Lookbook-->
</section>

<?php get_footer(); ?>
