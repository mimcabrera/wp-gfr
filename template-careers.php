<?php
/**
 * Template Name: Careers Page Template
 * The template for displaying about page.
 *
 * @package WordPress
 * @subpackage Gown_for_Rent
 * @since Gown for Rent 1.0
 */
get_header(); ?>

<!-- Start of Banner -->
	<article class="banner-content-section inner">
		<div id="banner">
			<div class="placeholder-bg">
				<div class="banner-item" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner-inner-page.jpg); background-size: cover; background-position: center;">
					<div class="mask-overlay"></div>
					<div class="banner-captions">
						<div class="container">
							<div class="col-xs-12 text-center">
								<h2 class="italic">Careers</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
	<!-- End of Banner -->
<section class="lookbook">
	<!-- Start of Lookbook-->
	<article class="content-section lookbook-section pad-30">
		<p class="text-center">We're not hiring... right now</p>
	</article>
</section>
	<!-- End of Lookbook-->

<?php get_footer(); ?>
