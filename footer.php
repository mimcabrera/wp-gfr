<?php
/**
 * The template for displaying the footer
 *
 *
 *
 * @package WordPress
 * @subpackage Gown_for_Rent
 * @since Gown for Rent 1.0
 */
?>
			<!-- Start of Newsletter -->
			<article class="content-section newsletter-section text-center">
				<h3 class="italic m-b-30">Sign up and get exclusive updates and promos</h3>
				<div class="container">
					<?php echo do_shortcode('[contact-form-7 id="80" title="Newsletter"]') ?>
				</div>
			</article>
			<!-- End of Newsletter -->
			<!-- Start of Footer -->
			<footer class="pad-t-50 pad-b-50">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-3">
							<h5 class="italic">Company</h5>
							<span class="line-divider white"></span>
							<ul>
								<li><a href="<?php echo get_site_url(); ?>/about">About</a></li>
								<li><a href="<?php echo get_site_url(); ?>/collections">Collections</a></li>
								<li><a href="<?php echo get_site_url(); ?>/careers">Careers</a></li>
								<li><a href="<?php echo get_site_url(); ?>/testimonials">Testimonials</a></li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-3">
							<h5 class="italic">Help</h5>
							<span class="line-divider white"></span>
							<ul>
								<li><a href="<?php echo get_site_url(); ?>/#">FAQ</a></li>
								<li><a href="<?php echo get_site_url(); ?>/contact-us">Contact Us</a></li>
								<li><a href="<?php echo get_site_url(); ?>/become-a-partner">Become a partner</a></li>
								<li><a href="<?php echo get_site_url(); ?>/sell-your-gown">Sell your gown</a></li>
								<li><a href="<?php echo get_site_url(); ?>/#">Privacy Policy</a></li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-3">
							<h5 class="italic">Showroom Location</h5>
							<span class="line-divider white"></span>
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3859.8497431075625!2d121.04120195039782!3d14.664467179389629!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTTCsDM5JzUyLjEiTiAxMjHCsDAyJzM2LjIiRQ!5e0!3m2!1sen!2sph!4v1467614801196" width="1920" height="200" frameborder="0" style="border:0; display: block; margin: auto" allowfullscreen></iframe>
							<a href="waze://?ll=14.664462,121.043396" target="_blank">Open in Waze</a>
						</div>
						<div class="col-xs-12 col-sm-3">
							<h5 class="italic">Connect with us</h5>
							<span class="line-divider white"></span>
								<ul class="social-accounts">
									<li><a href="//www.facebook.com/manilagowns" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a href="//www.instagram.com/gownforent" target="_blank"><i class="fa fa-instagram"></i></a></li>
								</ul>
						</div>
					</div>
				</div>
			</footer>
			<!-- Start of Copyright -->
			<div class="copyright">
				<p>copyright 2016 | all rights reserved</p>
			</div>
			<!-- End of Copyright -->
			<!-- End of Footer -->
		</section>
		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/smooth-scroll/10.0.0/js/smooth-scroll.min.js"></script>
		<script src="https://unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/app.min.js"></script>
	<?php wp_footer(); ?>
	</body>
</html>
