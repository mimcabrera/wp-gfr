<?php
/**
* @package WordPress
* @subpackage Gown_for_Rent
* @since Gown for Rent 1.0
*/

get_header('collection'); ?>
<!-- Start of Collection-->
<article class="content-section collection-section">
	<div class="container-fluid">
		<div class="col-md-2">
			<div class="sidebar-collection hidden-xs hidden-sm">
				<div class="search">
					<form action="<?php echo home_url(); ?>" id="search-form" method="get">
						<input type="text" name="s" id="s" placeholder="Search" value="" onblur="if(this.value=='')this.value=''"
						onfocus="if(this.value=='')this.value=''" />
						<input type="hidden" value="submit" />
					</form>
				</div>
				<h5 class="italic">Gowns</h5>
				<ul class="no-m">
					<li><a href="<?php echo home_url(); ?>/collections">All</a></li>
				</ul>
				<?php
				$post_type		= 'gowns';
				$taxonomy		= 'category';
				$orderby		= 'ASC';
				$show_count		= 0;
				$hide_empty		= 0;
				$pad_counts		= 0;
				$hierarchical	= 1;
				$exclude		= '1,24,28,29,42';
				$title			= ' ';

				$args = array(
					'post_type'			=> $post_type,
					'taxonomy'			=> $taxonomy,
					'orderby'			=> $orderby,
					'show_count'		=> $show_count,
					'hide_empty'		=> $hide_empty,
					'pad_counts'		=> $pad_counts,
					'hierarchical'	    => $hierarchical,
					'title_li'			=> $title,
					'exclude'			=> $exclude
				);

				wp_list_categories( $args )

				?>
				<h5 class="italic">Accessories</h5>
				<?php
				$post_type		= 'accessories';
				$taxonomy		= 'accessories_category';
				$orderby		= 'ASC';
				$show_count		= 0;
				$hide_empty		= 0;
				$pad_counts		= 0;
				$hierarchical	= 1;
				$title			= ' ';

				$args = array(
					'post_type'			=> $post_type,
					'taxonomy'			=> $taxonomy,
					'orderby'				=> $orderby,
					'show_count'		=> $show_count,
					'hide_empty'		=> $hide_empty,
					'pad_counts'		=> $pad_counts,
					'hierarchical'	=> $hierarchical,
					'title_li'			=> $title
				);

				wp_list_categories( $args )

				?>
				<a href="/category/exclusive-sale/"><h5 class="italic">Exclusive Sale</h5></a>
				<h5 class="italic">Gift Certificate</h5>
			</div>
		</div>
		<div class="col-md-10">
			<div class="collection-wrapper">
				<a class="toggle-button btn-gfr-default blue square">Open Categories</a>
				<!-- Start of Collection Items -->

				<div class="category-description">
					<!-- Start of Banner -->
					<article class="banner-content-section">
						<div id="banner">
							<div class="placeholder-bg">
								<div class="banner-item" style="background: url(<?php echo the_field('20k_package', 'option'); ?>); background-size: cover; background-position: top right; height: 22vw !important;"></div>
							</div>
						</div>
					</article>
					<!-- End of Banner -->
				</div>
				<article class="grid-content-section collection-items-section">
					<div class="row">
						<?php
						$this_category = get_category($cat);
						$slug = $this_category->category_nicename;
						?>
						<?php
						$posts = get_posts(array(
							'posts_per_page'		=> -1,
							'post_type'				=> 'gowns',
							'orderby'				=> 'title',
							'order'					=> 'ASC',
							'category_name'			=> $slug,
						));
						if( $posts ): ?>
						<?php foreach( $posts as $post ):
							setup_postdata( $post )
							?>
							<?php
							$thumbnail_image = get_field('thumbnail_image', $post->ID);
							$price = get_field('price', $post->ID);
							?>
							<div class="col-xs-6 col-sm-3">
									<div class="grid-item-container">
										<a href="<?php echo $post->guid ?>">
											<img src="<?php echo $thumbnail_image; ?>" alt="">
											<div class="grid-item-description">
												<div class="col-xs-12 no-pad text-center">
													<h5 class="italic no-m"><?php echo $post->post_title ?></h5>
													<?php if ( get_field( 'old_price' ) ): ?>
														<s><p class="price" style="color:#777">Php <?php the_field('old_price'); ?></p></s>
														<?php
														else :
															// no old price found
														endif;
														?>
														<p class="price">Php <?php echo $price; ?></p>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														<a href="<?php the_permalink(); ?>" class="btn-gfr-default dark-gray xs center xs-rent m-t-5 ">Rent Now</a>
													</div>
												</div>
											</a>
										</div>
									</div>
							<?php endforeach; ?>
							<?php wp_reset_postdata(); ?>
						<?php endif; ?>
					</div>
				</article>
				<!-- End of Collection Items -->
			</div>
		</div>
	</div>
</article>
<!-- End of Collection-->
<?php get_footer(); ?>
