<?php
/**
 * Template Name: Contact Us Page Template
 * The template for displaying about page.
 *
 * @package WordPress
 * @subpackage Gown_for_Rent
 * @since Gown for Rent 1.0
 */
get_header(); ?>
<!-- Start of Banner -->
<article class="banner-content-section inner">
	<div id="banner">
		<div class="placeholder-bg">
			<div class="banner-item" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner-inner-page.jpg); background-size: cover; background-position: center;">
				<div class="mask-overlay"></div>
				<div class="banner-captions">
					<div class="container">
						<div class="col-xs-12 text-center">
							<h2 class="italic">Contact Us</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<!-- End of Banner -->
<!-- Start of Contact Us-->
<article class="content-section contact-us pad-30" itemscope itemtype="http://schema.org/LocalBusiness">
	<div class="container m-t-20">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="row">
					<div class="col-xs-2"><h2 class="italic">01/</h2></div>
					<div class="col-xs-10"><h3 class="italic">Get in Touch</h3></div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>We're very approachable and would like to speak to you. Feel free to call, send us an email, Tweet us or simply complete the inquiry form.</p>
						<ul>
							<li>
								<div class="row">
									<div class="col-xs-1 text-center">
										<i class="fa fa-clock-o"></i>
									</div>
									<div class="col-xs-11">
										<p itemprop="openingHours" content="Mo,Tu,We,Fr,Sa, Su 9:00-18:00" datetime="Mo, Tu, We, Th, Fr, Sa, Su 09:00-18:00">Opening Hours : Mon-Fri 9AM to 8PM | Sat 9AM to 6PM | Sun 1PM to 6PM</p>
											<p>Holidays : Upon <a href="//fb.com/manilagowns/">Facebook</a> Announcement</p>
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-xs-1 text-center">
										<i class="fa fa-phone"></i>
									</div>
									<div class="col-xs-11">
										<a itemprop="telephone" content="+6329904957" href="tel:029904957">990 4957</a> / <a itemprop="telephone" content="+6327821180" href="tel:027821180">782 1180</a>
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-xs-1 text-center">
										<i class="fa fa-mobile"></i>
									</div>
									<div class="col-xs-11">
										<a itemprop="telephone" content="+6329983606102" href="tel:09983606102">Smart - 09983606102</a>
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-xs-1 text-center">
										<i class="fa fa-mobile"></i>
									</div>
									<div class="col-xs-11">
										<a itemprop="telephone" content="+6329174178891" href="tel:09174178891">Globe - 09174178891</a>
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-xs-1 text-center">
										<i class="fa fa-mobile"></i>
									</div>
									<div class="col-xs-11">
										<a itemprop="telephone" content="+6329209186466" href="tel:09209186466">(Viber, Telegram, Whatsapp, LINE) 09209186466</a>
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-xs-1 text-center">
										<i class="fa fa-envelope"></i>
									</div>
									<div class="col-xs-11">
										<a itemprop="email" content="sales@gownforrent.com" href="mailto:sales@gownforrent.com">sales@gownforrent.com</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="row">
					<div class="col-xs-2"><h2 class="italic">02/</h2></div>
					<div class="col-xs-10"><h3 class="italic">Send us a Message</h3></div>
					<?php echo do_shortcode('[contact-form-7 id="26" title="Contact Us"]') ?>
				</div>
			</div>
		</div>
		<div class="row m-t-50">
			<div class="col-xs-12">
				<div class="row m-b-20">
					<div class="col-xs-1"><h2 class="italic">03/</h2></div>
					<div class="col-xs-11"><h3 class="italic">Visit us</h3></div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3859.8497431075625!2d121.04120195039782!3d14.664467179389629!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTTCsDM5JzUyLjEiTiAxMjHCsDAyJzM2LjIiRQ!5e0!3m2!1sen!2sph!4v1467614801196" width="1920" height="500" frameborder="0" style="border:0; display: block; margin: auto" allowfullscreen></iframe>
						<a href="waze://?ll=14.664462,121.043396" target="_blank">Open in Waze</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<!-- End of Contact Us-->

<?php get_footer(); ?>
