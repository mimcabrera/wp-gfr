<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Gown_for_Rent
 * @since Gown for Rent 1.0
 */

get_header(); ?>
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<div class="container">
			<?php the_title( '<h6 class="entry-title">', '</h6>' ); ?>
			<?php the_content(); ?>
		</div>
	<?php endwhile;
		  endif; ?>
<?php get_footer(); ?>
