<?php
/**
 * Template Name: Thank You Page Template
 * The template for displaying about page.
 *
 * @package WordPress
 * @subpackage Gown_for_Rent
 * @since Gown for Rent 1.0
 */
get_header(); ?>
<section class="thank-you-section">
<div class="container">
	<div class="row">
		<div class="col-xs-12 text-center">
			<h1>Thank You!</h1>
			<p class="m-w-700">Our customer service will get intouch with you to confirm availability of item</p>
		</div>
	</div>
</div>
</section>
<?php get_footer(); ?>
