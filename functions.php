<?php

function register_gfr_menu() {
    register_nav_menus( array(
        'main-menu'       =>  'Main Menu',
        'footer-menu'     =>  'Footer Menu'
    ) );
}
//Add support for WordPress 3.0's custom menus
add_action( 'init', 'register_gfr_menu' );

add_theme_support( 'post-thumbnails' );


// Load different template for sub category
function sub_category_template() {

    // Get the category id from global query variables
    $cat = get_query_var('cat');

    if(!empty($cat)) {

        // Get the detailed category object
        $category = get_category($cat);

        // Check if it is sub-category and having a parent, also check if the template file exists
        // if( ($category->parent != '0') && (file_exists(TEMPLATEPATH . '/category-ten.php')) ) {
        if( ($cat == '226') && (file_exists(TEMPLATEPATH . '/category-ten.php')) ) {

            // Include the template for sub-catgeory
            include(TEMPLATEPATH . '/category-ten.php');
            exit;
        } if ( ($cat == '227') && (file_exists(TEMPLATEPATH . '/category-fifteen.php')) ) {

            // Include the template for sub-catgeory
            include(TEMPLATEPATH . '/category-fifteen.php');
            exit;
        } if ( ($cat == '228') && (file_exists(TEMPLATEPATH . '/category-twenty.php')) ) {

            // Include the template for sub-catgeory
            include(TEMPLATEPATH . '/category-twenty.php');
            exit;
        } if ( ($cat == '235') && (file_exists(TEMPLATEPATH . '/category-thirty.php')) ) {

            // Include the template for sub-catgeory
            include(TEMPLATEPATH . '/category-thirty.php');
            exit;
        } if ( ($cat == '225') && (file_exists(TEMPLATEPATH . '/category-wedding-packages.php')) ) {

            // Include the template for sub-catgeory
            include(TEMPLATEPATH . '/category-wedding-packages.php');
            exit;
        }
        return;
    }
    return;

}
add_action('template_redirect', 'sub_category_template');
