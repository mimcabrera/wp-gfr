<?php
/**
 * Template Name: Become a Partner Page Template
 * The template for displaying about page.
 *
 * @package WordPress
 * @subpackage Gown_for_Rent
 * @since Gown for Rent 1.0
 */
get_header(); ?>
<section class="book-an-appointment">
	<!-- Start of Banner -->
	<article class="banner-content-section inner">
		<div id="banner">
			<div class="placeholder-bg">
				<div class="banner-item" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner-inner-page.jpg); background-size: cover; background-position: center;">
					<div class="mask-overlay"></div>
					<div class="banner-captions">
						<div class="container">
							<div class="col-xs-12 text-center">
								<h2 class="italic">Become a Partner</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
	<!-- End of Banner -->
	<article class="form-content-section">
		<div class="container">
			<?php echo do_shortcode('[contact-form-7 id="44" title="Become a partner"]') ?>
		</div>
	</article>
</section>
<?php get_footer(); ?>
