<?php
/**
 * Template Name: Testimonials Page Template
 * The template for displaying testimonials page.
 *
 * @package WordPress
 * @subpackage Gown_for_Rent
 * @since Gown for Rent 1.0
 */
get_header(); ?>
<!-- Start of Banner -->
<article class="banner-content-section inner">
	<div id="banner">
		<div class="placeholder-bg">
			<div class="banner-item" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner-inner-page.jpg); background-size: cover; background-position: center;">
				<div class="mask-overlay"></div>
				<div class="banner-captions">
					<div class="container">
						<div class="col-xs-12 text-center">
							<h2 class="italic">Testimonials</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<!-- End of Banner -->
<!-- Start of Testimonials-->
<section class="testimonials">
	<article class="content-section testimonials pad-30">
		<div class="container m-t-20">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php else : ?>
			<?php endif; ?>
		</div>
	</article>
</section>
<!-- End of Testimonials-->

<?php get_footer(); ?>
