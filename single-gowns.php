<?php
	/*
	Single Post Template: Gowns
	Description: This part is optional, but helpful for describing the Post Template
	*/
?>

<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Gown_for_Rent
 * @since Gown for Rent 1.0
 */

get_header(); ?>
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<section class="collections-inner-page">
			<article class="content-section product-section">
				<div class="container-fluid no-pad">
					<div class="col-sm-6 no-pad">
						<div class="product-item-image">
							<div id="sync1" class="owl-carousel">
								<?php
								// check if the repeater field has rows of data
								if( have_rows('product_images') ):
								 	// loop through the rows of data
								    while ( have_rows('product_images') ) : the_row();
								        // display a sub field value
								?>
								        <img src=" <?php the_sub_field('image'); ?>" alt="">
								<?php
							 	endwhile;
								else :
								    // no rows found
								endif;
								?>
							</div>
							<div id="sync2" class="owl-carousel">
								<?php
								// check if the repeater field has rows of data
								if( have_rows('product_images') ):
								 	// loop through the rows of data
								    while ( have_rows('product_images') ) : the_row();
								        // display a sub field value
								?>
								        <img src=" <?php the_sub_field('image'); ?>" alt="">
								<?php
							 	endwhile;
								else :
								    // no rows found
								endif;
								?>
							</div>
						</div>
					</div>
					<div class="col-sm-6 pad-l-20">
						<div class="product-item-details">
							<?php the_title( '<h4 class="italic product-name m-b-10">', '</h4>' ); ?>
							<p class="product-description">Category: <?php the_category(' '); ?></p>
							<span class="line-divider m-t-30 m-b-30"></span>
							<p><?php the_content(); ?></p>
							<?php if ( get_field( 'old_price' ) ): ?>
							<s><h5 class="product-price m-b-10" style="color:#777">Php <?php the_field('old_price'); ?></h5></s>
							<?php
								else :
										// no old price found
								endif;
							?>
							<h4 class="product-price m-b-30">Php <?php the_field('price'); ?></h4>
							<?php $url = get_site_url() . "/rent-a-gown/?product=" . $post->ID . "&type=" . $post->post_type; ?>
							<a href="<?php echo $url ?>" class="btn-gfr-default dark-gray xs">Rent now</a>
						</div>
					</div>
				</div>
			</article>
		</section>
	<?php wp_reset_postdata(); ?>
	<?php endwhile;
		  endif; ?>
<?php get_footer(); ?>
